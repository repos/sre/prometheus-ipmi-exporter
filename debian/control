Source: prometheus-ipmi-exporter
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Daniel Swarbrick <dswarbrick@debian.org>,
Section: net
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-golang,
               golang-any,
               golang-github-go-kit-log-dev,
               golang-github-prometheus-client-golang-dev,
               golang-github-prometheus-common-dev,
               golang-github-prometheus-exporter-toolkit-dev (>= 0.8.0),
               golang-gopkg-alecthomas-kingpin.v2-dev,
               golang-gopkg-yaml.v2-dev,
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/go-team/packages/prometheus-ipmi-exporter
Vcs-Git: https://salsa.debian.org/go-team/packages/prometheus-ipmi-exporter.git
Homepage: https://github.com/prometheus-community/ipmi_exporter
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/prometheus-community/ipmi_exporter

Package: prometheus-ipmi-exporter
Architecture: any
Pre-Depends: ${misc:Pre-Depends},
Depends: adduser,
         daemon | systemd-sysv,
         freeipmi-tools,
         ${misc:Depends},
         ${shlibs:Depends},
Built-Using: ${misc:Built-Using},
Description: Prometheus exporter for IPMI devices
 Prometheus exporter for Intelligent Platform Management Interface (IPMI)
 device sensor metrics. Supports local IPMI devices (e.g., /dev/ipmi0), or
 remote devices via Remote Management Control Protocol (RMCP). When using
 RMCP, a single exporter can be used to monitor a large number of IPMI
 devices by passing a target hostname as a parameter.
 .
 This exporter relies on tools from the FreeIPMI suite for the actual IPMI
 implementation.
